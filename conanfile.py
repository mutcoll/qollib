from conans import ConanFile, CMake


class QollibConan(ConanFile):
    name = "qollib"
    version = "0.0.2"
    license = "LGPL"
    author = "jmmut jomutlo@gmail.com"
    url = "https://bitbucket.org/mutcoll/qollib"
    description = """Quality of life library. Basic independent tools that every program will need,
like logging, segfault detection, backtrace reporting, conversion to strings, tests and assertions"""
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    exports_sources = "src/*", "CMakeLists.txt", "test_package/*"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder=".")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["qollib"]

