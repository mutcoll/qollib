/**
 * @file test_stacktrace.cpp
 * @date 2020-12-16
 */

#include <iostream>

#include "randomize/exception/SignalHandler.h"
#include "randomize/exception/StackTracedException.h"

auto showSignalHandler() -> void {
    try {
        int *i = nullptr;
        std::cout << "dereferencing an invalid pointer..." << std::endl;
        std::cout << *i << std::endl;
        std::cout << "shouldn't have reached this point!" << std::endl;
        exit(1);
    } catch (std::exception &e) {
        std::cout << "caught exception. See how it knows the current function:\n" << e.what() << std::endl;
    }
}

auto intermediateFunction() -> void {
    showSignalHandler();
}

int main(int argc, char **argv) {
    randomize::exception::SignalHandler::activate();

    intermediateFunction();
    return 0;
}

/* This is the full verbatim output of this program:

# In linux:

dereferencing an invalid pointer...
caught exception. See how it knows the current function:
exception wrapper for a raised signal 11.
stack trace:
  ./bin/test_basic : exception::StackTracedException::StackTracedException()+0x76
  ./bin/test_basic : exception::SignalHandler::handleSignal(int)+0x58
  /lib/x86_64-linux-gnu/libc.so.6 : ()+0x46210
  ./bin/test_basic : showSignalHandler()+0x41
  ./bin/test_basic : intermediateFunction()+0xd
  ./bin/test_basic : main()+0x1d
  /lib/x86_64-linux-gnu/libc.so.6 : __libc_start_main()+0xf3
  ./bin/test_basic : _start()+0x2e
*/

