/**
 * @file test_testsuite.cpp
 * @date 2020-12-16
 */

#include <iostream>

#include "randomize/test/TestSuite.h"

int main(int argc, char **argv) {
    using namespace randomize::test;

    TestSuite{"showcasing TestSuite assertions", {
            Test{[]() {
                assert_equals(1 + 5, 2);
            }},
            Test{[]() {
                assert_equals(1 + 2, 4, "message to explain why 3 should equal 4");
            }},
            Test{[]() {
                assert_equals(1 + 1, 2, "test that is working and is not reported");
            }}
    }};

    TestSuite{"showcasing TestSuite exceptions", {
            Test{[]() {
                assert_throws([]() {
                    auto thisDoesNotThrow = 2;
                });
            }},
            Test{[]() {
                throw std::runtime_error{"this is an unrelated exception in some code"};
            }}
    }};
    return 0;
}


/* this is the output of the above program:

failed test 'showcasing TestSuite assertions': /home/jmmut/Documents/conan/qollib/test_package/test_testsuite.cpp:15:30: assertion failed: should be equal:
  actual:   6
  expected: 2

failed test 'showcasing TestSuite assertions': /home/jmmut/Documents/conan/qollib/test_package/test_testsuite.cpp:18:30: assertion failed: should be equal:
  actual:   3
  expected: 4
  explanation: message to explain why 3 should equal 4

TestSuite: 2/3 [showcasing TestSuite assertions] tests failed (indices are: [0, 1])
failed test 'showcasing TestSuite exceptions': /home/jmmut/Documents/conan/qollib/test_package/test_testsuite.cpp:27:30: assertion failed: should have thrown an exception.
failed test 'showcasing TestSuite exceptions' (at /home/jmmut/Documents/conan/qollib/test_package/test_testsuite.cpp:33:14): threw exception: this is an unrelated exception in some code
TestSuite: 2/2 [showcasing TestSuite exceptions] tests failed (indices are: [0, 1])

 */
