/**
 * @file TestSuite.cpp
 * @author jmmut
 * @date 2015-10-10.
 */

#include "TestSuite.h"
#include "randomize/log/log.h"
#include "randomize/string/String.h"

namespace randomize::test {

unsigned long TestSuite::totalFailedTestsCount = 0;

#if __cplusplus > 201703L
TestSuite::TestSuite(std::string name, std::initializer_list<Test> tests) {
    int i = 0;
    for (auto &test : tests) {
        try {
            test.func();
        } catch (AssertionException & except) {
            std::cout << "failed test '" << name
                      << "': "
                      << except.what()
                      << std::endl;
            failedTests.insert(i);
        } catch (std::exception &except) {
            std::cout << "failed test '" << name
                      << "' (at " << test << "): threw exception: "
                      << except.what()
                      << std::endl;
            failedTests.insert(i);
        }
        ++i;
    }
    totalFailedTestsCount += failedTests.size();
    auto indices = std::string{""};
    if (not failedTests.empty()) {
        indices = " (indices are: " + randomize::string::to_string(failedTests) + ")";
    }
    std::cout << "TestSuite: " << failedTests.size() << "/" << tests.size()
              << " [" << name << "] tests failed" << indices << std::endl;

}
#else // __cplusplus > 201703L

TestSuite::TestSuite(std::string name, std::initializer_list<std::function<void()>> tests) {
    int i = 0;
    for (auto &func : tests) {
        try {
            func();
        } catch (std::exception &except) {
            std::cerr << "test [" << name << "][" << i << "]: " << except.what() << std::endl;
            failedTests.insert(i);
        }
        ++i;
    }
    std::cerr << "TestSuite: " << failedTests.size() << "/" << tests.size() << " [" << name << "] tests failed" << std::endl;

    if (not failedTests.empty()) {
        totalFailedTestsCount += failedTests.size();
        std::cerr << "failed [" << name << "] tests indices are: " << randomize::string::to_string(failedTests) << std::endl;
    }
}
#endif // __cplusplus > 201703L
unsigned long TestSuite::countFailed() {
    return failedTests.size();
}

unsigned long TestSuite::countTotalFailed() {
    return totalFailedTestsCount;
}

}
