#ifndef QOLLIBS_TESTSUITE2_H
#define QOLLIBS_TESTSUITE2_H

/**
 * @file TestSuite.h
 * @date 2015-10-10.
 */

#include <utility>
#include <vector>
#include <set>
#include <functional>
#include <iostream>
#include <string>
#include "randomize/exception/StackTracedException.h"
#include "randomize/log/log.h"

#if __cplusplus > 201703L

#include <source_location>

namespace randomize::test {

inline std::ostream &operator<<(std::ostream &out, const std::source_location &location) {
    return out << location.file_name() << ":"
               << location.line() << ":"
               << location.column();
}

struct Test {
    Test(std::function<void()> func,
         const std::source_location location = std::source_location::current())
            :func{std::move(func)}, location{location} {
    }
    std::function<void()> func;
    std::source_location location;
    friend std::ostream &operator<<(std::ostream &out, const Test &test) {
        return out << test.location;
    }
};

/**
 * to use like this:
```
int main(int argc, char **argv) {
    using namespace randomize::test;

    TestSuite{"showcasing TestSuite assertions", {
            Test{[]() {
                assert_equals(1 + 5, 2);
            }},
            Test{[]() {
                assert_equals(1 + 2, 4, "message to explain why 3 should equal 4");
            }},
            Test{[]() {
                assert_equals(1 + 1, 2, "test that is working and is not reported");
            }}
    }};

    TestSuite{"showcasing TestSuite exceptions", {
            Test{[]() {
                assert_throws([]() {
                    auto thisDoesNotThrow = 2;
                });
            }},
            Test{[]() {
                throw std::runtime_error{"this is an unrelated exception in some code"};
            }}
    }};
    return 0;
}
```
 * You can use the name to group unit tests and differentiate TestSuites.
 * To mark a test as failed, throw an exception, which is what the assert_* functions do (below).
 */
class TestSuite {
public:
    TestSuite(std::string name, std::initializer_list<Test>);
    unsigned long countFailed();
    static unsigned long countTotalFailed();
private:
    std::set<int> failedTests;
    static unsigned long totalFailedTestsCount;
};

class AssertionException : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

inline void assert_equals(auto actual, auto expected, std::string message = "",
                   std::source_location location = std::source_location::current()) {
    if (not (actual == expected)) {
        std::stringstream ss;
        auto extraMessage = std::string{message}.empty()?
                    std::string{""} : "\n  explanation: " + message;
        ss << location << ": assertion failed: should be equal:"
           << "\n  actual:   " << actual
           << "\n  expected: " << expected
           << extraMessage
           << "\n";
        throw AssertionException{ss.str()};
    }
}

inline void assert_throws(auto functionThatShouldThrow, std::string message = "",
                          std::source_location location = std::source_location::current()) {
    bool threw = false;
    try {
        functionThatShouldThrow();
    } catch (std::exception &e) {
        LOG_DEBUG("expected exception's message: %s", e.what());
        threw = true;
    }
    if (not threw) {
        std::stringstream ss;
        ss << location << ": assertion failed: should have thrown an exception.";
        if (not message.empty()) {
            ss << " " << message;
        }
        throw AssertionException(ss.str());
    }
}

} //randomize::test

#else // __cplusplus > 201703L

namespace randomize::test {
/**
 * to use like this:
```
int main() {
    randomize::test::TestSuite{"test name", {
            []() {
                ASSERT_EQUALS_OR_THROW(1 + 1, 2);
            }, []() {
                ASSERT_EQUALS_OR_THROW(1, 2);
            }
    }};

    return 0;
}
```
 * You can use the name to group unit tests and differentiate TestSuites.
 * To mark a test as failed, throw an exception, which is what the ASSERT_* macros do (below).
 */
class TestSuite {
public:
    TestSuite(std::string name, std::initializer_list<std::function<void()>>);
    unsigned long countFailed();
    static unsigned long countTotalFailed();
private:
    std::set<int> failedTests;
    static unsigned long totalFailedTestsCount;
};

#define ASSERT_OR_THROW(expression) ASSERT_OR_THROW_MSG(expression, "")

#define ASSERT_OR_THROW_MSG(expression, message) \
    if (not (expression)) \
        throw randomize::exception::StackTracedException()\
                << __FILE__ ":" << __LINE__ << (": assertion failed. This was expected to be true: ("  #expression  ")\n") << message


#define ASSERT_EQUALS_OR_THROW(actual, expected) ASSERT_EQUALS_OR_THROW_MSG(actual, expected, "")

#define ASSERT_EQUALS_OR_THROW_MSG(actual, expected, message) \
    if (const auto &_actualResult{actual}; true)              \
        if (const auto &_expectedResult{expected}; not ((_actualResult) == (_expectedResult))) \
            throw randomize::exception::StackTracedException()\
                    << __FILE__ << ":" << __LINE__            \
                    << (": assertion (" #actual " == " #expected ") failed:\n")                \
                    << _actualResult << "\n==\n" << _expectedResult                            \
                    << "\n" << message

} // randomize::test

#endif // __cplusplus > 201703L

#endif // QOLLIBS_TESTSUITE2_H
