#ifndef QOLLIBS_STACKTRACEDEXCEPTION_H
#define QOLLIBS_STACKTRACEDEXCEPTION_H

/**
 * @file StackTracedException.h
 * @date 2016-03-26.
 */


// in case the user doesn't have execinfo.h, or unix's signals: disable backtracing on segfaults:
//#define DO_NOT_BACKTRACE


#if !defined DO_NOT_BACKTRACE && !defined WIN32

#include <signal.h>
#include <errno.h>
#include <execinfo.h>
#include <sys/types.h>
#include <unistd.h>
#include <cxxabi.h>
#endif // DO_NOT_BACKTRACE

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace randomize::exception {

/**
 * exception that carries the call stack in the message. Use as a regular exception:

    if (badcondition) {
        throw StackTracedException("something went wrong");
    }

 * or use as a stream (like std::cout) for complex messages:

    if (badcondition) {
        throw StackTracedException() << 9 << " out of " << 10 << " dentists disapprove your code!";
    }
 */
class StackTracedException : public std::exception {
private:
    std::stringstream message;
    std::stringstream stacktrace;
    mutable std::string copy;


public:
    explicit StackTracedException();
    explicit StackTracedException(const std::string &message);

    const char *what() const noexcept override;

    template<typename T>
    void append(T addition) {
        message << addition;
    }
};

void print_stacktrace(std::ostream &out, unsigned int max_frames = 63);

}

template <typename T>
randomize::exception::StackTracedException &&operator<<(randomize::exception::StackTracedException &&e, T addition) {
    e.append(addition);
    return std::move(e);
}
template <typename T>
randomize::exception::StackTracedException &operator<<(randomize::exception::StackTracedException &e, T addition) {
    e.append(addition);
    return e;
}



#endif
