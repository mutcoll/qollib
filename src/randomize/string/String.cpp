/**
 * @file Strings.cpp
 * @date 2020-12-15
 */

#include <algorithm>
#include "String.h"

namespace randomize::strings {

std::vector<std::string> split(const std::string &str, const std::string &endChars) {
    std::vector<std::string> strings;
    std::string::const_iterator first = str.begin();
    auto last = first;
    for (; last != str.end(); last++) {
        if (std::find(endChars.begin(), endChars.end(), *last) != endChars.end()) {
            if (first != last) {
                strings.emplace_back(first, last);
            }
            first = last + 1;
        }
    }
    if (first != last) {
        strings.emplace_back(first, last);
    }
    return strings;
}

}
