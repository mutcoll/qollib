#ifndef QOLLIB_STRING_H
#define QOLLIB_STRING_H

/**
 * @file Strings.h
 * @date 2016-02-15
 */

#include <string>
#include <vector>
#include <sstream>

namespace randomize::string {

/**
 * Splits by any of the single characters in `separators`. Won't split by the whole string.
 */
std::vector<std::string> split(const std::string &splitable, const std::string &separators);


/**
 * A container is anything that has defined `.empty()`, `.begin()` and `.end()` and can be iterated with them.
 * To print the inner types, the operator<< has to be defined for them.
 */
template<typename C>
std::string to_string(const C &container, std::string open, std::string separator, std::string close) {
    if (container.empty()) {
        return open + close;
    } else {
        std::stringstream ss;
        ss << open;
        auto it = container.begin();
        ss << *it;
        ++it;
        for (; it != container.end(); ++it) {
            ss << separator << *it;
        }
        ss << close;
        return ss.str();
    }
}

template<typename C>
std::string to_string(const C &container) {
    return to_string(container, "[", ", ", "]");
}

}



#endif //QOLLIB_STRING_H
