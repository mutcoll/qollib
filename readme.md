# Randomize's Quality of Life Library (qollib)

Qollib is a super simple library that contains utilities that most projects will likely need, but is lacking from the standard library included with C++:

- Be able to throw an exception that reports the stack trace
- Automatically detect a segmentation fault and throw an exception with the stack trace
- Basic logging (whose logging level you can easily change at runtime. I'm looking at you, java)
- Basic conversion of containers to string (seriously, WTF, how many hours of developers' time have gone into this?)
- Basic testing with assertions

To get a quick taste of these features, look at 
[a stacktrace test](test_package/test_stacktrace.cpp)
or [a test with assertions](test_package/test_testsuite.cpp).

## Using this library

The easiest way is using conan + cmake. You can probably pass without cmake, look at [other conan generators](https://docs.conan.io/en/latest/integrations/build_system.html) to see how to tell your build system to use dependencies from conan.

### TL;DR ; I know how conan goes

Add my conan remote:
```
conan remote add randomize-conan https://randomize.jfrog.io/artifactory/api/conan/randomize-conan
```

And put this:
```
qollib/0.0.2
```
in the `[requires]` section of your `conanfile.txt`

### Longer explanation

If you are using conan + cmake, put this in a `conanfile.txt` in the root folder of your project:
```
[requires]
qollib/0.0.2

[generators]
cmake
```

and then setup conan:
```
# feel free to use virtualenvs, but this is easier
sudo pip3 install conan

# tell your conan installation to look for my conan packages (non conan official)
conan remote add randomize-conan https://randomize.jfrog.io/artifactory/api/conan/randomize-conan
```

add this your `CMakeLists.txt`:
```
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
```
and make sure to link against `${CONAN_LIBS}` in your `target_link_libraries()`.
See [the CMakeLists.txt used in the package tests](test_package/CMakeLists.txt) for a complete 
working cmake configuration, which builds c++ source files from `test_package/`.

And now prepare the dependency for building your project and build it:
```
mkdir build && cd build
conan install .. --build missing
cmake ..
make
```

Look at [conan docs](https://docs.conan.io/en/latest/getting_started.html) if you are not using 
cmake, for more gentle introductions or for other troubleshooting.

